package vn.edu.stu.quanlybaitap;

import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import Database.database;

public class MainActivity2 extends AppCompatActivity {
    database database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        //  tạo database
        database = new database(this,"loaiBaiTap.sqlite",null, 1);
        database.QueryData("CREATE TABLE IF NOT EXISTS loaiBaiTap(Id INTEGER PRIMARY KEY AUTOINCREMENT, TenLoai VARCHAR(200)");

    }
}