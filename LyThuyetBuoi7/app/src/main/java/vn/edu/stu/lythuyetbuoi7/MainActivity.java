package vn.edu.stu.lythuyetbuoi7;

import static android.support.constraint.solver.widgets.analyzer.RunGroup.index;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import vn.edu.stu.lythuyetbuoi7.model.Anhem;

public class MainActivity extends AppCompatActivity {

    EditText etMa, etTen, etNamSinh;
    Button btnThem;
    ListView lvAe;
    ArrayAdapter<Anhem> Adapter;
    Anhem aeChon = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addControls();
        addEvents();
    }

    private void addControls() {
        etMa = findViewById(R.id.etMa);
        etTen = findViewById(R.id.etTen);
        etNamSinh = findViewById(R.id.etNamSinh);
        btnThem = findViewById(R.id.btnThem);
        lvAe = findViewById(R.id.lvAe);
        Adapter = new ArrayAdapter<>(
                MainActivity.this,
                android.R.layout.simple_list_item_1
        );
        lvAe.setAdapter(Adapter);
    }

    private void addEvents() {
        btnThem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                xuLyThem();
            }
        });

      lvAe.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
          @Override
          public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
              xuLyChon(index);
          }
      });

        lvAe.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                xuLyXoa(index);
                return true;
            }
        });
    }

    private void xuLyThem() {
        int ma = Integer.parseInt(etMa.getText().toString());
        String ten = etTen.getText().toString();
        int namSinh = Integer.parseInt(etNamSinh.getText().toString());

        if(aeChon == null) {

            Adapter.add(new Anhem(ma, ten, namSinh));
        }else {
           aeChon.setMa(ma);
            aeChon.setTen(ten);
            aeChon.setNamSinh(namSinh);
            Adapter.notifyDataSetChanged();
            aeChon == null;
            btnThem.setText("THEM ANH EM");
        }
        etMa.setText("");
        etTen.setText("");
        etNamSinh.setText("");

        etMa.requestFocus();
    }

    public void xuLyChon(int index) {
        if(index >= 0 && index < Adapter.getCount()){
            aeChon = Adapter.getItem(index);
            etTen.setText(aeChon.getTen() + "");
            etNamSinh.setText(aeChon.getNamSinh() + "");
            etMa.setText(aeChon.getMa());
            etMa.requestFocus()
            btnThem.setText("SUA ANH EM");
        }
    }

    public void xuLyXoa(int index) {
        if(index >= 0 && index < Adapter.getCount()){
            Anhem ae = Adapter.getItem(index);

            Adapter.remove(ae);

        }
    }
}