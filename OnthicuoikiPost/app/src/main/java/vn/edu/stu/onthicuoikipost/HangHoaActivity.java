package vn.edu.stu.onthicuoikipost;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import vn.edu.stu.onthicuoikipost.model.HangHoa;

public class HangHoaActivity extends AppCompatActivity {
//    final String SERVER = "http://document.fitstu.net/de1/";
    final String SERVER = "http://192.168.1.215/de1/";
    EditText edtLoaihanghoa;
    Button btngetData;
    ArrayList<HangHoa> dsHangHoa;
    ArrayAdapter<HangHoa> adapterHH;
    ListView lvHangHoa;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hang_hoa);
        addControls();
        addEvents();
    }

    private void addControls() {
        edtLoaihanghoa = findViewById(R.id.edtloaiHangHoa);
        btngetData = findViewById(R.id.btngetData);
        lvHangHoa = findViewById(R.id.lvHanghoa);
        dsHangHoa = new ArrayList<>();
        adapterHH = new ArrayAdapter<>(HangHoaActivity.this, android.R.layout.simple_list_item_1, dsHangHoa);
        lvHangHoa.setAdapter(adapterHH);
    }

    private void addEvents() {
        btngetData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                xuLyGetData();
            }
        });
    }

    private void xuLyGetData() {
        RequestQueue requestQueue = Volley.newRequestQueue(HangHoaActivity.this);
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(
                        HangHoaActivity.this,
                        response,
                        Toast.LENGTH_LONG
                ).show();
                try {
                    // Server trả về một chuỗi response có dạng đối tượng JSON, nên ta ép nó thành JSONObject
                    dsHangHoa.clear();
                    JSONArray jsonArray = new JSONArray(response);
                    int len = jsonArray.length();
                    for(int i = 0 ; i < len; i++) {
                        JSONObject jsonObject= jsonArray.getJSONObject(i);
                        int ma= Integer.parseInt(jsonObject.getString("ma"));
                        String ten= jsonObject.getString("ten");
                        double gia = Double.parseDouble(jsonObject.getString("gia"));
                        dsHangHoa.add(new HangHoa(ma, ten, gia));
                    }

                } catch (Exception ex) {
                }
            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(HangHoaActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        };
        Uri.Builder builder = Uri.parse(SERVER + "posthanghoa.php").buildUpon();
//        builder.appendQueryParameter("loaihanghoa", edtLoaihanghoa.getText().toString());
        String url = builder.build().toString();
        StringRequest request = new StringRequest(
                Request.Method.POST,
                url,
                responseListener,
                errorListener
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("loaihanghoa", edtLoaihanghoa.getText().toString());
                return params;
            }
        };
        request.setRetryPolicy(
                new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                )
        );
        requestQueue.add(request);
    }
}