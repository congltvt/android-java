package vn.edu.stu.onthicuoikipost.model;

public class HangHoa {
    private int ma;
    private String ten;
    private double gia;


    public HangHoa(String ten, double gia) {
        this.ten = ten;
        this.gia = gia;
    }

    public HangHoa(int ma, String ten, double gia) {
        this.ma = ma;
        this.ten = ten;
        this.gia = gia;
    }

    public int getMa() {
        return ma;
    }

    public void setMa(int ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public double getGia() {
        return gia;
    }

    public void setGia(double gia) {
        this.gia = gia;
    }



    @Override
    public String toString() {
        return "Mã: " + ma +'\n'+
                "Tên: " + ten + '\n' +
                "Giá: " + gia;
    }
}
