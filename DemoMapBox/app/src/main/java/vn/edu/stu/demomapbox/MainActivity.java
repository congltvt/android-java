package vn.edu.stu.demomapbox;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.BitmapFactory;
import android.os.Bundle;

import com.mapbox.geojson.Point;
import com.mapbox.geojson.Polygon;
import com.mapbox.maps.CameraOptions;
import com.mapbox.maps.EdgeInsets;
import com.mapbox.maps.MapView;
import com.mapbox.maps.Style;
import com.mapbox.maps.plugin.annotation.AnnotationConfig;
import com.mapbox.maps.plugin.annotation.AnnotationPlugin;
import com.mapbox.maps.plugin.annotation.AnnotationPluginImplKt;
import com.mapbox.maps.plugin.annotation.generated.PointAnnotationManager;
import com.mapbox.maps.plugin.annotation.generated.PointAnnotationManagerKt;
import com.mapbox.maps.plugin.annotation.generated.PointAnnotationOptions;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    MapView mapView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addControls();

    }

    private void addControls() {
        mapView = findViewById(R.id.mapView);
        mapView.getMapboxMap().loadStyleUri(
                Style.SATELLITE,
                new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        xuLyNapBanDo();
                    }
                }
        );
    }

    private void xuLyNapBanDo() {
        // STU: 10.738242625107251, 106.67794694095484
        Point pointSTU = Point.fromLngLat(106.67794694095484, 10.738242625107251);
        // TDTU: 10.732563696311752, 106.69919385638768
        Point pointTDTU = Point.fromLngLat(106.69919385638768, 10.732563696311752);
        // HMCUT: 10.772144058649591, 106.65790515873525
        Point pointHCMUT = Point.fromLngLat(106.65790515873525, 10.772144058649591);

        AnnotationPlugin annotationPlugin = AnnotationPluginImplKt.getAnnotations(mapView);
        PointAnnotationManager manager = PointAnnotationManagerKt.createPointAnnotationManager(
                annotationPlugin,
                new AnnotationConfig()
        );
        PointAnnotationOptions optionsSTU = new PointAnnotationOptions()
                .withPoint(pointSTU)
                .withTextField("STU nè")
                .withIconImage(BitmapFactory.decodeResource(this.getResources(), R.drawable.red_marker));
        manager.create(optionsSTU);
        PointAnnotationOptions optionsTDTU = new PointAnnotationOptions()
                .withPoint(pointTDTU)
                .withTextField("TDTU nè")
                .withIconImage(BitmapFactory.decodeResource(this.getResources(), R.drawable.red_marker));
        manager.create(optionsTDTU);
        PointAnnotationOptions optionsHCMUT = new PointAnnotationOptions()
                .withPoint(pointHCMUT)
                .withTextField("HCMUT nè")
                .withIconImage(BitmapFactory.decodeResource(this.getResources(), R.drawable.red_marker));
        manager.create(optionsHCMUT);

//        CameraOptions cameraOptions = new CameraOptions.Builder()
//                .center(pointSTU)
//                .zoom(15.0)
//                .pitch(0.0)
//                .bearing(0.0)
//                .build();
//        mapView.getMapboxMap().setCamera(cameraOptions);

        List<Point> points = new ArrayList<>();
        points.add(pointSTU);
        points.add(pointTDTU);
        points.add(pointHCMUT);
        List<List<Point>> coordinates = new ArrayList<>();
        coordinates.add(points);
        Polygon polygon = Polygon.fromLngLats(coordinates);
        EdgeInsets edgeInsets = new EdgeInsets(100.0, 100.0, 100.0, 100.0);
        CameraOptions cameraOptions = mapView.getMapboxMap().cameraForGeometry(
                polygon,
                edgeInsets,
                0.0,
                0.0
        );
        mapView.getMapboxMap().setCamera(cameraOptions);
    }
}