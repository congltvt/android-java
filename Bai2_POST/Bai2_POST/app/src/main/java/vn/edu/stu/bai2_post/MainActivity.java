package vn.edu.stu.bai2_post;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import vn.edu.stu.model.SinhVien;

public class MainActivity extends AppCompatActivity {

    final String SERVER = "http://192.168.1.215/WebService/api_post.php";

    EditText tvMa, tvTen;
    Button btnLuu;
    ArrayList<SinhVien> dssv;
    ArrayAdapter<SinhVien> adapter;
    ListView lvsv;
    SinhVien chon = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addControls();
        hienthiDanhSach();
        addEvents();
    }

    private void addControls() {
        tvMa = findViewById(R.id.txtMaSv);
        tvTen = findViewById(R.id.txtTenSv);
        btnLuu = findViewById(R.id.btnLuu);
        lvsv = findViewById(R.id.lvSv);
        dssv = new ArrayList<>();
        adapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, dssv);
        lvsv.setAdapter(adapter);
    }


    private void addEvents() {
        btnLuu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (chon == null) {
                    //Thêm mới
                    int ma = Integer.parseInt(tvMa.getText().toString());
                    String ten = tvTen.getText().toString();
                    SinhVien sv = new SinhVien(ma, ten);
                    xuLyThemSV(sv);
                    tvMa.setText("");
                    tvTen.setText("");
                    tvMa.requestFocus();
                } else {
                    //Cập nhật
                    String ten = tvTen.getText().toString();
                    chon.setTensv(ten);
                    xuLyCapNhatSV(chon);
                    chon = null;
                    tvMa.setText("");
                    tvTen.setText("");
                    tvMa.setEnabled(true);
                    tvMa.requestFocus();
                }
            }
        });
        lvsv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position >= 0 && position < dssv.size()) {
                    chon = dssv.get(position);
                    tvMa.setText(chon.getMasv() + "");
                    tvTen.setText(chon.getTensv());
                    tvMa.setEnabled(false);
                }
            }
        });
        lvsv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (position >= 0 && position < dssv.size()) {
                    SinhVien sv = dssv.get(position);
                    xuLyXoaSV(sv);
                }
                return true;
            }
        });
    }

    private void hienthiDanhSach() {
        //Hàng đợi  các request lên server
        RequestQueue requestQueue = Volley.newRequestQueue(
                MainActivity.this
        );

        //Lắng nghe kết quả trả về
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    dssv.clear();
                    //Server trả về một chuỗi có dạng mảng JSON,
                    //nên ta ép nó thành JSONArray rồi lặp
                    //trên Array để lấy ra từng JSONObject
                    JSONArray jsonArray = new JSONArray(response);
                    int len = jsonArray.length();
                    for (int i = 0; i < len; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        int ma = jsonObject.getInt("masv");
                        String ten = jsonObject.getString("tensv");
                        dssv.add(new SinhVien(ma, ten));
                    }
                    adapter.notifyDataSetChanged();
                } catch (Exception ex) {

                }
            }
        };

        //Lắng nghe lỗi trả về(thường là lỗi kết nối)
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(
                        MainActivity.this,
                        error.getMessage(),
                        Toast.LENGTH_LONG
                ).show();
            }
        };

        //Tạo url đến service
        Uri.Builder builder = Uri.parse(SERVER).buildUpon();
        // Chèn thêm tham số url, dùng trong phuong thức $_GET
        // builder.appendQueryParameter("action", "getall");
        String url = builder.build().toString();
        StringRequest request = new StringRequest(
                Request.Method.POST,//Nếu dùng $_POST thì đổi thành POST
                url,
                responseListener,
                errorListener
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("action", "getall");
                return params;
            }
        };
        // Volley có xu hướng thực hiện nhiều cuộc gọi đến máy chủ chậm // vì nó không nhận được phản hồi tù yêu cầu đầu tiên,
        // nên cần cấu hình thông tin thủ lại (Retry)
        // DEFAULT TIMEOUT MS: Thời gian chờ tối đa trong mỗi lần thủ lại. Mặc // định 2500ms.
        // DEFAULT MAX RETRIES: Số lần thủ lại tối đa. Mặc định 1.
        // DEFAULT BACKOFF MULT: Hệ số được xác định thời gian theo hàm mũ
        // được gán cho socket trong mỗi lần thủ lại. Mặc định 1.0f
        request.setRetryPolicy(
                new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                )
        );
        requestQueue.add(request);
    }

    private void xuLyThemSV(final SinhVien sv) {
        RequestQueue requestQueue = Volley.newRequestQueue(
                MainActivity.this
        );
        Response.Listener<String> responseListener =
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //Server trả về một chuỗi có dạng đôi tượng,
                            //JSON, nên ta ép nó thành JSONObject
                            JSONObject jsonObject = new JSONObject(response);
                            boolean result = jsonObject.getBoolean("message");
                            if (result) {
                                Toast.makeText(
                                        MainActivity.this,
                                        "Thêm thành công",
                                        Toast.LENGTH_SHORT
                                ).show();
                                hienthiDanhSach();
                            } else {
                                Toast.makeText(
                                        MainActivity.this,
                                        "Thêm thất bại",
                                        Toast.LENGTH_SHORT
                                ).show();
                            }
                        } catch (Exception ex) {

                        }
                    }
                };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(
                        MainActivity.this,
                        error.getMessage(),
                        Toast.LENGTH_LONG
                ).show();
            }
        };

        Uri.Builder builder = Uri.parse(SERVER).buildUpon();
        // builder.appendQueryParameter("action", "insert");
        //builder.appendQueryParameter("masv", sv.getMasv() + "");
        //builder.appendQueryParameter("tensv", sv.getTensv());
        String url = builder.build().toString();
        StringRequest request = new StringRequest(
                Request.Method.POST,
                url,
                responseListener,
                errorListener
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("action", "insert");
                params.put("masv", sv.getMasv() + "");
                params.put("tensv", sv.getTensv());
                return params;
            }
        };
        request.setRetryPolicy(
                new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                )
        );
        requestQueue.add(request);
    }

    private void xuLyXoaSV(final SinhVien sv) {
        RequestQueue requestQueue = Volley.newRequestQueue(
                MainActivity.this
        );
        Response.Listener<String> responseListener =
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //Server trả về một chuỗi có dạng đôi tượng,
                            //JSON, nên ta ép nó thành JSONObject
                            JSONObject jsonObject = new JSONObject(response);
                            boolean result = jsonObject.getBoolean("message");
                            if (result) {
                                Toast.makeText(
                                        MainActivity.this,
                                        "Xóa thành công",
                                        Toast.LENGTH_SHORT
                                ).show();
                                hienthiDanhSach();
                            } else {
                                Toast.makeText(
                                        MainActivity.this,
                                        "Xóa thất bại",
                                        Toast.LENGTH_SHORT
                                ).show();
                            }
                        } catch (Exception ex) {

                        }
                    }
                };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(
                        MainActivity.this,
                        error.getMessage(),
                        Toast.LENGTH_LONG
                ).show();
            }
        };

        Uri.Builder builder = Uri.parse(SERVER).buildUpon();
        // builder.appendQueryParameter("action", "delete");
        // builder.appendQueryParameter("masv", sv.getMasv() + "");
        String url = builder.build().toString();
        StringRequest request = new StringRequest(
                Request.Method.POST,
                url,
                responseListener,
                errorListener
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("action", "delete");
                params.put("masv", sv.getMasv() + "");
                return params;
            }
        };
        request.setRetryPolicy(
                new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                )
        );
        requestQueue.add(request);
    }

    private void xuLyCapNhatSV(final SinhVien chon) {
        RequestQueue requestQueue = Volley.newRequestQueue(
                MainActivity.this
        );
        Response.Listener<String> responseListener =
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //Server trả về một chuỗi có dạng đôi tượng,
                            //JSON, nên ta ép nó thành JSONObject
                            JSONObject jsonObject = new JSONObject(response);
                            boolean result = jsonObject.getBoolean("message");
                            if (result) {
                                Toast.makeText(
                                        MainActivity.this,
                                        "Sửa thành công",
                                        Toast.LENGTH_SHORT
                                ).show();
                                hienthiDanhSach();
                            } else {
                                Toast.makeText(
                                        MainActivity.this,
                                        "Sửa thất bại",
                                        Toast.LENGTH_SHORT
                                ).show();
                            }
                        } catch (Exception ex) {

                        }
                    }
                };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(
                        MainActivity.this,
                        error.getMessage(),
                        Toast.LENGTH_LONG
                ).show();
            }
        };

        Uri.Builder builder = Uri.parse(SERVER).buildUpon();
        //builder.appendQueryParameter("action", "update");
        //builder.appendQueryParameter("masv", chon.getMasv() + "");
        //builder.appendQueryParameter("tensv", chon.getTensv());
        String url = builder.build().toString();
        StringRequest request = new StringRequest(
                Request.Method.POST,
                url,
                responseListener,
                errorListener
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("action", "update");
                params.put("masv", chon.getMasv() + "");
                params.put("tensv", chon.getTensv());
                return params;
            }
        };
        request.setRetryPolicy(
                new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                )
        );
        requestQueue.add(request);
    }

}