package vn.edu.stu.nguyenhaotoan_thick_dh51904677.model;

public class PhongBan {
    private String tenpb;
    private int mapb;
    private String mota;

    public PhongBan(String tenpb, int mapb, String mota) {
        this.tenpb = tenpb;
        this.mapb = mapb;
        this.mota = mota;
    }

    public PhongBan() {
    }

    public String getMota() {
        return mota;
    }

    public void setMota(String mota) {
        this.mota = mota;
    }

    public String getTenpb() {
        return tenpb;
    }

    public void setTenpb(String tenpb) {
        this.tenpb = tenpb;
    }

    public int getMapb() {
        return mapb;
    }

    public void setMapb(int mapb) {
        this.mapb = mapb;
    }
}
