package vn.edu.stu.nguyenhaotoan_thick_dh51904677.model;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import vn.edu.stu.nguyenhaotoan_thick_dh51904677.R;

public class AdapterNhanVien extends BaseAdapter {
    private Activity context;
    private int layout;
    private List<NhanVien> listNV;

    public AdapterNhanVien(Activity context, int layout, List<NhanVien> listNV) {
        this.context = context;
        this.layout = layout;
        this.listNV = listNV;
    }

    @Override
    public int getCount() {
        return listNV.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    private class viewHolder {
        TextView tvma, tvten, tvns, tvgt;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        viewHolder holder;
        if (view == null) {
            holder = new viewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(layout, null);
            holder.tvma = (TextView) view.findViewById(R.id.tvma);
            holder.tvten = (TextView) view.findViewById(R.id.tvten);
            holder.tvns = (TextView) view.findViewById(R.id.tvns);
            holder.tvgt = (TextView) view.findViewById(R.id.tvgt);
            view.setTag(holder);
        } else {
            holder = (viewHolder) view.getTag();
        }
        NhanVien nv = listNV.get(i);
        holder.tvma.setText(holder.tvma.getText().toString() +String.valueOf(nv.getManv()));
        holder.tvten.setText(holder.tvten.getText().toString() +nv.getTennv().toString());
        holder.tvns.setText(holder.tvns.getText().toString()+ nv.getNgaysinh());

        if(nv.getGioitinh()==0) {
            holder.tvgt.setText(holder.tvgt.getText().toString()+ "Nữ");
        }else{
            holder.tvgt.setText(holder.tvgt.getText().toString()+ "Nam");
        }
        return view;
    }
}
