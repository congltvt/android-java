package vn.edu.stu.nguyenhaotoan_thick_dh51904677.model;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import vn.edu.stu.nguyenhaotoan_thick_dh51904677.R;

public class AdapterLoaiPB extends BaseAdapter {
    private Activity context;
    private int layout;
    private List<PhongBan> listLoai;
    public AdapterLoaiPB(Activity context, int layout, List<PhongBan> listLoai) {
        this.context = context;
        this.layout = layout;
        this.listLoai = listLoai;
    }
    @Override
    public int getCount() {
        return listLoai.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    private class viewHolder {
        TextView tvma;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        AdapterLoaiPB.viewHolder holder;
        if (view == null) {
            holder = new AdapterLoaiPB.viewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(layout, null);
            holder.tvma = (TextView) view.findViewById(R.id.tvmaloaisp);
            view.setTag(holder);
        } else {
            holder = (AdapterLoaiPB.viewHolder) view.getTag();
        }
        PhongBan loai = listLoai.get(i);
        holder.tvma.setText(String.valueOf(loai.getTenpb()));
        return view;
    }
}
