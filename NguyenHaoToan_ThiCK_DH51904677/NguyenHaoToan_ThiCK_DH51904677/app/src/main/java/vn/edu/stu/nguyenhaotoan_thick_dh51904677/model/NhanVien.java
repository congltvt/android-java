package vn.edu.stu.nguyenhaotoan_thick_dh51904677.model;

public class NhanVien {
    private String tennv;
    private int manv;
    private String ngaysinh;
    private int gioitinh;
    private int maphongban;

    public NhanVien( int manv,String tennv, String ngaysinh, int gioitinh) {
        this.tennv = tennv;
        this.manv = manv;
        this.ngaysinh = ngaysinh;
        this.gioitinh = gioitinh;
    }

    public NhanVien() {
    }

    public NhanVien(String tennv, String ngaysinh, int gioitinh, int maphongban) {
        this.tennv = tennv;
        this.ngaysinh = ngaysinh;
        this.gioitinh = gioitinh;
        this.maphongban = maphongban;
    }

    public NhanVien(String tennv, int manv, String ngaysinh, int gioitinh, int maphongban) {
        this.tennv = tennv;
        this.manv = manv;
        this.ngaysinh = ngaysinh;
        this.gioitinh = gioitinh;
        this.maphongban = maphongban;
    }

    public String getTennv() {
        return tennv;
    }

    public void setTennv(String tennv) {
        this.tennv = tennv;
    }

    public String getNgaysinh() {
        return ngaysinh;
    }

    public void setNgaysinh(String ngaysinh) {
        this.ngaysinh = ngaysinh;
    }

    public int getGioitinh() {
        return gioitinh;
    }

    public void setGioitinh(int gioitinh) {
        this.gioitinh = gioitinh;
    }

    public int getMaphongban() {
        return maphongban;
    }

    public void setMaphongban(int maphongban) {
        this.maphongban = maphongban;
    }

    public int getManv() {
        return manv;
    }

    public void setManv(int manv) {
        this.manv = manv;
    }
}
