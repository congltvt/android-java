package vn.edu.stu.nguyenhaotoan_thick_dh51904677;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ThemNV extends AppCompatActivity {
    Button btnthem,btnhuy;
    EditText etma,etten,etns,etgt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_them_nv);
        addControls();
        addEvents();
    }

    private void addControls() {
        btnthem=findViewById(R.id.btnThem);
        btnhuy=findViewById(R.id.btnHuy);
        etma=findViewById(R.id.etMa);
        etten=findViewById(R.id.etTen);
        etns=findViewById(R.id.etNS);
        etgt=findViewById(R.id.etGT);
    }

    private void addEvents() {
        btnthem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        btnhuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}