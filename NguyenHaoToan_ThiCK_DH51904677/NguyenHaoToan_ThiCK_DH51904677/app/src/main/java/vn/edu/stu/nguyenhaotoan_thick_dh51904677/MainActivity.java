package vn.edu.stu.nguyenhaotoan_thick_dh51904677;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import vn.edu.stu.nguyenhaotoan_thick_dh51904677.model.AdapterLoaiPB;
import vn.edu.stu.nguyenhaotoan_thick_dh51904677.model.AdapterNhanVien;
import vn.edu.stu.nguyenhaotoan_thick_dh51904677.model.NhanVien;
import vn.edu.stu.nguyenhaotoan_thick_dh51904677.model.PhongBan;
import vn.edu.stu.nguyenhaotoan_thick_dh51904677.model.ThongTin;

public class MainActivity extends AppCompatActivity {
    String getnv = "https:/dichvuseogiatot.com/api/getAllNhanVien.php";
    String getpb = "https:/dichvuseogiatot.com/api/getAllPhongBan.php";

    ListView lvNV;
    Button btnNV,btnPB,btnThem;
    int a=-1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addControls();
        getnv(getnv);
        //getpb(getpb);
        addEvents();
    }

    private void addControls() {
        btnNV=findViewById(R.id.btnAll);
        btnPB=findViewById(R.id.btnPL);
        btnThem=findViewById(R.id.btnThem);
        lvNV = findViewById(R.id.listview1);
        ThongTin.listnv =new ArrayList<>();
        ThongTin.adapterNhanVien=new AdapterNhanVien(MainActivity.this,R.layout.listviewsp_row,ThongTin.listnv);
        ThongTin.listnvtheopb= new ArrayList<>();
        ThongTin.adapterNhanVienPB= new AdapterNhanVien(MainActivity.this,R.layout.listviewsp_row,ThongTin.listnvtheopb);
        ThongTin.listpb= new ArrayList<>();
        ThongTin.listpb = new ArrayList<>();
        ThongTin.adapterLoaiPB = new AdapterLoaiPB(MainActivity.this,R.layout.listviewloai_row,ThongTin.listpb);
        lvNV.setAdapter(ThongTin.adapterNhanVien);
    }

    private void addEvents() {
        btnNV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                xulyNV();
            }
        });
        btnPB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                xulyPB();
            }
        });
        btnThem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,ThemNV.class);
                startActivity(intent);
            }
        });
    }

    private void xulyNV() {
        ThongTin.function = true;
        ListView lvPB;
        Dialog dialog = new Dialog(MainActivity.this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.listviewloai_row);
        lvPB = (ListView) dialog.findViewById(R.id.lvloai);
        lvPB.setAdapter(ThongTin.adapterLoaiPB);
        ImageButton btnnew = (ImageButton) dialog.findViewById(R.id.btnnew);
        ImageButton btnback = (ImageButton) dialog.findViewById(R.id.btnback);
        lvPB.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });
        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        dialog.show();
    }

    private void xulyPB() {

    }

    private void getnv(String server) {
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, server, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject object = response.getJSONObject(i);
                                if(a==-1){
                                    ThongTin.listnv.add(new NhanVien(
                                                Integer.parseInt(object.getString("id")),
                                                object.getString("tennv"),
                                                object.getString("birthday"),
                                                Integer.parseInt(object.getString("gioitinh")
                                            ))
                                    );
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                       ThongTin.adapterNhanVien.notifyDataSetChanged();
                        a=0;
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }
   /* private void getpb(String server) {
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, server, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject object = response.getJSONObject(i);
                                if(a==0){
                                    ThongTin.listpb.add(new PhongBan(
                                            object.getString("ten_pb"),
                                            Integer.parseInt(object.getString("id_pb")),
                                            object.getString("des")
                                            )
                                    );
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        ThongTin.adapterLoaiPB.notifyDataSetChanged();
                        a=1;
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }*/
}