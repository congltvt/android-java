package vn.edu.stu.thuchanhbuoi6bt1.la06_quanlydatphong;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import vn.edu.stu.thuchanhbuoi6bt1.R;
import vn.edu.stu.thuchanhbuoi6bt1.model.DatPhong;

public class MainActivity extends AppCompatActivity {

    ArrayAdapter<DatPhong> adapter;
    ListView lvDatPhong;
    FloatingActionButton fabThem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addControls();
        addEvents();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(adapter != null){
            adapter.notifyDataSetInvalidated();;
        }
    }

    private void addControls() {
        adapter = new ArrayAdapter<>(
                MainActivity.this,
                android.R.layout.simple_list_item_1,
                Dulieu.dsDatPhong
        );

        lvDatPhong = findViewById(R.id.lvDatPhong);
        lvDatPhong.setAdapter(adapter);
        fabThem = findViewById(R.id.fabThem);

        registerForContextMenu(lvDatPhong);
    }

    private void addEvents() {
        fabThem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(
                        MainActivity.this, DatPhongActivity.class
                );
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mnu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.mnuThem:
                Intent intent = new Intent(
                        MainActivity.this,
                        DatPhongActivity.class
                );
                startActivity(intent);
                break;
            case R.id.mnuThoat:
                finish();
                System.exit(0);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if(v.getId() == R.id.lvDatPhong){
            getMenuInflater().inflate(R.menu.mnu_datphong, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo ) item.getMenuInfo();
                int index = info.position;
        switch (item.getItemId()){
            case R.id.mnuSua:
                Intent intent = new Intent(
                        MainActivity.this,
                        DatPhongActivity.class
                );
                intent.putExtra("INDEX", index);
                startActivity(intent);
                break;
            case R.id.mnuXoa:
                Dulieu.xoaDatPhong(index);
                adapter.notifyDataSetChanged();
                break;
            default:
                break;
        }
        return super.onContextItemSelected(item);
    }
}