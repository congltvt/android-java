package vn.edu.stu.thuchanhbuoi6bt1.la06_quanlydatphong;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Calendar;

import vn.edu.stu.thuchanhbuoi6bt1.R;
import vn.edu.stu.thuchanhbuoi6bt1.model.DatPhong;
import vn.edu.stu.thuchanhbuoi6bt1.util.FormatUtil;
import vn.edu.stu.thuchanhbuoi6bt1.util.RandomUtil;

public class DatPhongActivity extends AppCompatActivity {
    TextView txtMa, txtNgayDat;
    EditText txtTenNguoiDat, txtSoDem;
    ImageButton btnDatePicker;
    Button btnLuu;
    Calendar calender;
    DatPhong chon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dat_phong);

        addControls();
        getIntentData();
        addEvents();
    }

    private void addControls() {
        txtMa = findViewById(R.id.txtMa);
        txtNgayDat = findViewById(R.id.txtNgayDat);
        txtTenNguoiDat = findViewById(R.id.txtTenNguoiDat);
        txtTenNguoiDat = findViewById(R.id.txtTenNguoiDat);
        btnDatePicker = findViewById(R.id.btnDatePicker);
        txtSoDem = findViewById(R.id.txtSoDem);
        btnLuu = findViewById(R.id.btnLuu);
        calender = Calendar.getInstance();
        chon = null;
    }

    private void getIntentData() {
        Intent intent = getIntent();
        if(intent.hasExtra("INDEX")){
            int index = intent.getIntExtra("INDEX", -1);
            chon = Dulieu.layDatPhong(index);
        }
        if(chon != null){
            txtMa.setText(chon.getMa());
            txtTenNguoiDat.setText(chon.getTenNguoiDat());
            calender.setTime(chon.getNgayDat());
            txtNgayDat.setText(FormatUtil.formatDate(chon.getNgayDat()));
            txtSoDem.setText(chon.getSoDem());
        }else{
            txtMa.setText(RandomUtil.getAlphaNumericString(8));
            txtTenNguoiDat.requestFocus();

            calender.add(Calendar.DATE, 2);
        }
    }

    private void addEvents() {
        btnDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                xuLyChonNgay();
            }
        });
        btnLuu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                xuLyLuu();
            }
        });
    }

    private void xuLyLuu() {
        if(chon != null){
            chon.setTenNguoiDat(txtTenNguoiDat.getText().toString());
            chon.setNgayDat(calender.getTime());
            chon.setSoDem(Integer.parseInt(txtSoDem.getText().toString()));
        }else{
            chon = new DatPhong();
            chon.setMa(txtMa.getText().toString());
            chon.setTenNguoiDat(txtTenNguoiDat.getText().toString());
            chon.setNgayDat(calender.getTime());
            chon.setSoDem(Integer.parseInt(txtSoDem.getText().toString()));
            Dulieu.themDatPhong(chon);
        }
        finish();
    }

    private void xuLyChonNgay() {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                calender.set(Calendar.YEAR, year);
                calender.set(Calendar.MONTH, monthOfYear);
                calender.set(Calendar.DATE, dayOfMonth);
                txtNgayDat.setText(FormatUtil.formatDate(calender.getTime()));
            }
        };
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                DatPhongActivity.this,
                listener,
                calender.get(calender.YEAR),
                calender.get(Calendar.MONTH),
                calender.get(Calendar.DATE)
        );

        long oneDay = 24 * 60 * 60 *1000L;
        datePickerDialog.getDatePicker().setMinDate(
                System.currentTimeMillis() + 2 * oneDay);
                datePickerDialog.show();
    }

}