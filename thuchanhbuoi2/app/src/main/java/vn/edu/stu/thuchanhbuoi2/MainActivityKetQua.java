package vn.edu.stu.thuchanhbuoi2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class MainActivityKetQua extends AppCompatActivity {
    public static ArrayList<String> kbg = new ArrayList<String>(){
        {
            add("KÉO");
            add("BÚA");
            add("GIẤY");
        }
    };

    TextView txtBanRa, txtMayRa, txtKetQua;
    Button btnTroLai;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_ket_qua);

        addControls();
        addEvents();
        layThongTinBanCho();
    }

    private void addControls() {
        txtBanRa = findViewById(R.id.txtBanRa);
        txtMayRa = findViewById(R.id.txtMayRa);
        txtKetQua = findViewById(R.id.txtKetQua);
        btnTroLai = findViewById(R.id.btnTroLai);
    }

    private void addEvents() {
        btnTroLai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void layThongTinBanCho() {
        Intent intent = getIntent();
        if(intent.hasExtra("BANRA")){
            String banRa = intent.getStringExtra("BANRA");
            if(banRa == ""){
                txtKetQua.setText("KHÔNG CÓ THÔNG TIN");
            }else {
                txtBanRa.setText("Bạn ra: " + banRa);
                int iBanRa = kbg.indexOf(banRa);

                int iMayRa = new Random().nextInt(kbg.size());
                String mayRa = kbg.get(iMayRa);
                txtMayRa.setText("Máy ra: " + mayRa);

                int kq = iBanRa - iMayRa;
                if (kq == 0) txtKetQua.setText("KẾT QUẢ: HÒA");
                else if (kq == 1 || kq == -2)
                    txtKetQua.setText("Kết quả: Bạn Thắng");
                else txtKetQua.setText("Kết quả: Bạn Thua");
            }
        }else {
            txtBanRa.setText("Không có thông tin");
        }
    }
}