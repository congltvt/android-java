package vn.edu.stu.thuchanhbuoi2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    Button btnKeo, btnBua, btnGiay, btnNghiChoi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addControls();
        addEvents();
    }

    private void addControls() {
        btnKeo = findViewById(R.id.btnKeo);
        btnBua = findViewById(R.id.btnBua);
        btnGiay = findViewById(R.id.btnGiay);
        btnNghiChoi = findViewById(R.id.btnNghiChoi);
    }

    private void addEvents() {
        btnKeo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                xuLyRa(view);
            }
        });

        btnBua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                xuLyRa(view);
            }
        });

        btnGiay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                xuLyRa(view);
            }
        });

        btnNghiChoi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void xuLyRa(View view){
        String banRa = ((Button)view).getText().toString().toUpperCase();
        Intent intent = new Intent(
                MainActivity.this,
                MainActivityKetQua.class
        );
        intent.putExtra("BANRA", banRa);
        startActivity(intent);
    }
}