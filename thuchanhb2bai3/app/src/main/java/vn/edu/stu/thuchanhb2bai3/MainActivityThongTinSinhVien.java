package vn.edu.stu.thuchanhb2bai3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivityThongTinSinhVien extends AppCompatActivity {
    TextView txtThongtin;
    Button btnTroLai;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_thong_tin_sinh_vien);
        addControls();
        addEvents();
        getDataFromIntent();
    }

    private void addControls() {
        txtThongtin = findViewById(R.id.txtThongTin);
        btnTroLai = findViewById(R.id.btnTroLai);
    }

    private void addEvents() {
        btnTroLai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if(intent.hasExtra("SINHVIEN")){
            SinhVien sv = (SinhVien) intent.getSerializableExtra("SINHVIEN");
            txtThongtin.setText(sv.toString());
        }
    }
}