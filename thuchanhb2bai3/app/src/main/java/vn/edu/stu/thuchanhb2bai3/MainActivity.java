package vn.edu.stu.thuchanhb2bai3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText etMSV, etHoTen, etNamSinh;
    Button btnGuiThongTin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addControls();
        addEvents();
    }

    private void addControls() {
        etMSV = findViewById(R.id.etMSV);
        etHoTen = findViewById(R.id.etHoTen);
        etNamSinh = findViewById(R.id.etNamSinh);
        btnGuiThongTin = findViewById(R.id.btnGuiThongTin);
    }

    private void addEvents() {
        btnGuiThongTin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msv = etMSV.getText().toString();
                String hoten = etHoTen.getText().toString();
                int namsinh = Integer.parseInt(etNamSinh.getText().toString());
                SinhVien sv = new SinhVien(msv, hoten, namsinh);
                Intent intent = new Intent(
                        MainActivity.this, MainActivityThongTinSinhVien.class
                );
                intent.putExtra("SINHVIEN", sv);
                startActivity(intent);
            }
        });
    }
}