package vn.edu.stu.thuchanhb2bai3;

import java.io.Serializable;

public class SinhVien implements Serializable {
    private String msv;
    private String hoTen;
    private int namSinh;

    public SinhVien(String msv, String hoTen, int namSinh) {
        this.msv = msv;
        this.hoTen = hoTen;
        this.namSinh = namSinh;
    }

    @Override
    public String toString() {
        return "Mã Sinh Viên: " +
                 msv + "\nHọ và tên: "  +
                 hoTen + "\nNăm sinh: " +
                 namSinh ;
    }


}
