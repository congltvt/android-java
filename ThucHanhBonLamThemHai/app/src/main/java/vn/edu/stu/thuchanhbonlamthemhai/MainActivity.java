package vn.edu.stu.thuchanhbonlamthemhai;

import android.content.Intent;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

import vn.edu.stu.thuchanhbonlamthemhai.model.Nhanvien;

public class MainActivity extends AppCompatActivity {


    ArrayList<Nhanvien> dsNhanvien;
    ArrayAdapter<Nhanvien> adapter;
    ListView lvDSNhanvien;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addControls();
        addEvents();
    }

    private void addControls() {
        adapter = new ArrayAdapter(
                MainActivity.this,
                dsNhanvien
        );
        String ma = dsNhanvien.toString(0);
        String ten = dsNhanvien.toString(1);
        String sdt =dsNhanvien.toString(2);
        ArrayList.add(new Nhanvien(ma, ten,sdt));
        lvDSNhanvien = findViewById(R.id.lvDSNhanVien);
        lvDSNhanvien.setAdapter(adapter);
    }

    private void addEvents() {
//        btnLuu.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String ma = txtMa.getText().toString();
//                String ten = txtTen.getText().toString();
//                String sdt = txtSdt.getText().toString();
//                Nhanvien nv = new Nhanvien(ma, ten, sdt);
//                dsNhanvien.add(nv);
//
//                adapter.notifyDataSetChanged();
//                txtMa.setText("");
//                txtTen.setText("");
//                txtSdt.setText("");
//                txtMa.requestFocus();
//            }
//        });



        lvDSNhanvien.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position , long id) {
                if(position >= 0 && position < dsNhanvien.size()){
                    dsNhanvien.remove(position);
                    adapter.notifyDataSetChanged();
                }
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menunhanvien,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            //Chuyển qua màn hình thêm sinh viên
            case R.id.themnv:
                Intent intent = new Intent(MainActivity.this, ThemNhanVienActivity.class);
                startActivity(intent);
                break;

            case R.id.suanv:
                Intent intent1 = new Intent(MainActivity.this, SuaNhanVienActivity.class);
                startActivity(intent1);
                break;


            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}