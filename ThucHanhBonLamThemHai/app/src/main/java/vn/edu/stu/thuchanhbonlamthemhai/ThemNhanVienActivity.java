package vn.edu.stu.thuchanhbonlamthemhai;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import vn.edu.stu.thuchanhbonlamthemhai.model.Nhanvien;

public class ThemNhanVienActivity extends AppCompatActivity {
    EditText txtMathem, txtTenthem, txtSdtthem;
    Button btnLuu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_them_nhan_vien);

        addControls();
        addEvents();
    }

    private void addControls() {
        txtMathem = findViewById(R.id.txtMathem);
        txtTenthem = findViewById(R.id.txtTenthem);
        txtSdtthem = findViewById(R.id.txtSDTthem);
        btnLuu = findViewById(R.id.btnLuu);
    }

    private void addEvents() {
        btnLuu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ma = txtMathem.getText().toString().trim();
                String ten = txtTenthem.getText().toString().trim();
                String sdt = txtSdtthem.getText().toString().trim();


                if(ma.equals("")|| ten.equals("")||sdt.equals("")){
                    Toast.makeText(ThemNhanVienActivity.this, "Hãy nhập chính xác", Toast.LENGTH_SHORT).show();
                }else {

                     Nhanvien nhanvien = TaoNhanvien();

                    Intent intent = new Intent(ThemNhanVienActivity.this, MainActivity.class);
                    startActivity(intent);

                    Toast.makeText(ThemNhanVienActivity.this,  "Đã thêm thành công", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private Nhanvien TaoNhanvien(){
        String ma = txtMathem.getText().toString().trim();
        String ten = txtTenthem.getText().toString().trim();
        String sdt = txtSdtthem.getText().toString().trim();

        Nhanvien nhanvien = new Nhanvien(ma, ten, sdt);

        return nhanvien;
    }
}