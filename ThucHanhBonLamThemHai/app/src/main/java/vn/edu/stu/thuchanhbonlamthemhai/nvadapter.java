package vn.edu.stu.thuchanhbonlamthemhai;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import vn.edu.stu.thuchanhbonlamthemhai.model.Nhanvien;

public class nvadapter extends BaseAdapter {

    private MainActivity context;

    private ArrayList<Nhanvien> ArraylistNhanvien;

    public nvadapter(MainActivity context, ArrayList<Nhanvien> arraylistNhanvien) {
        this.context = context;
        ArraylistNhanvien = arraylistNhanvien;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater ) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.listnv, null);

        TextView tvMa = view.findViewById(R.id.tvMa);

        TextView tvTen = view.findViewById(R.id.tvTen);

        TextView tvSdt = view.findViewById(R.id.tvSdt);


        Nhanvien nhanvien = ArraylistNhanvien.get(position);
        tvMa.setText(nhanvien.getMa()+ "");
        tvTen.setText(nhanvien.getTen()+ "");
        tvSdt.setText(nhanvien.getSdt()+ "");

        return view;
    }
}
