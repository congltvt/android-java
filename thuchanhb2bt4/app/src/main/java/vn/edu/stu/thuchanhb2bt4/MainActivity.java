package vn.edu.stu.thuchanhb2bt4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {
    ImageButton imgBtnKeo, imgBtnGiay, imgBtnBua;
    Button btnNghiChoi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addControls();
        addEvents();
    }

    private void addControls() {
        imgBtnKeo = findViewById(R.id.btnKeo);
        imgBtnGiay = findViewById(R.id.btnGiay);
        imgBtnBua = findViewById(R.id.btnBua);
        btnNghiChoi = findViewById(R.id.btnNghiChoi);
    }

    private void addEvents() {
        imgBtnKeo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                xuLyRa(view);
            }
        });

        imgBtnGiay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                xuLyRa(view);
            }
        });

        imgBtnBua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                xuLyRa(view);
            }
        });

        btnNghiChoi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void xuLyRa(View view){
        String banRa = ((ImageButton)view).getTag().toString().toUpperCase();
        Intent intent = new Intent(
                MainActivity.this, MainActivityKetQua.class
        );
        intent.putExtra("BANRA", banRa);
        startActivity(intent);
    }
}