package vn.edu.stu.thuchanhb2bt4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class MainActivityKetQua extends AppCompatActivity {
    public static ArrayList<String> kbg = new ArrayList<String>() {
        {
            add("drawable/keo.png");
            add("drawable/bua.png");
            add("drawable/giay.png");
        }
    };
    TextView txtBanRa, txtMayRa, txtKetQua;
    ImageView imgViewBanRa, imgViewMayRa;
    Button btnTroLai;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_ket_qua);
        addControls();
        addEvents();
        layThongTinBanCho();
    }

    private void addControls() {
        txtBanRa = findViewById(R.id.tvBanRa);
        txtMayRa = findViewById(R.id.tvMayRa);
        txtKetQua = findViewById(R.id.tvKetQua);
        btnTroLai = findViewById(R.id.btnTroLai);

        imgViewBanRa = findViewById(R.id.imvBanRa);
        imgViewMayRa = findViewById(R.id.imvMayRa);
    }

    private void addEvents() {
        btnTroLai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void layThongTinBanCho() {
        Intent intent = getIntent();
        if (intent.hasExtra("BANRA")) {
            String banra = intent.getStringExtra("BANRA");

            //imvBanRa=intent.getParcelableExtra("BANRA");
            if (banra == "") {
                txtKetQua.setText("Không có thông tin.");
            } else {
                int ibanra = Integer.parseInt(banra);
                txtBanRa.setText("Bạn ra :");
                if (ibanra == 0)
                    imgViewBanRa.setImageResource(R.drawable.keo);
                else if (ibanra == 1)
                    imgViewBanRa.setImageResource(R.drawable.bua);
                else
                    imgViewBanRa.setImageResource(R.drawable.giay);

                int imayra = new Random().nextInt(kbg.size());
                //String mayra = kbg.get(imayra);
                txtMayRa.setText("Máy ra : ");

                if (imayra == 0)
                    imgViewMayRa.setImageResource(R.drawable.keo);
                else if (imayra == 1)
                    imgViewMayRa.setImageResource(R.drawable.bua);
                else
                    imgViewMayRa.setImageResource(R.drawable.giay);

                int kq = ibanra - imayra;
                if (kq == 0) {
                    txtKetQua.setText("Kết Quả : Hòa");
                } else if (kq == 1 || kq == -2)
                    txtKetQua.setText("Kết Quả : Bạn Thắng");
                else
                    txtKetQua.setText("Kết Quả : Bạn Thua");
            }
        }
    }
}