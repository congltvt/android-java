package vn.edu.stu.lythuyetb2bai2;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    EditText etN;
    Button btnGuiN;
    TextView txtDSFibo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addControls();
        addEvents();
    }

    private void addControls() {
        etN = findViewById(R.id.etN);
        btnGuiN = findViewById(R.id.btnGuiN);
        txtDSFibo = findViewById(R.id.txtDSFibo);
    }

    private void addEvents() {
        btnGuiN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(
                        MainActivity.this, MainActivityManHinh2.class
                );
                int n = Integer.parseInt(etN.getText().toString());
                intent.putExtra("N", n);
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == 1){
            if(resultCode == 2){
                ArrayList<Integer> dsFibo = data.getIntegerArrayListExtra("DSFIBO");
                String result = "";
                for (int fibo : dsFibo){
                    result += fibo + "\n";
                }
                txtDSFibo.setText(result);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}