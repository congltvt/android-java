package vn.edu.stu.quanlynhanvien.model;

public class ThongTinCaNhan implements Serializable{
        /**
         *
         */
        private static final long serialVersionUID = 1L;
        private String ma;
        private String ten;
        public ThongTinCaNhan(String ma, String ten) {
            super();
            this.ma = ma;
            this.ten = ten;
        }
        public ThongTinCaNhan() {
            super();
        }
        public String getMa() {
            return ma;
        }
        public void setMa(String ma) {
            this.ma = ma;
        }
        public String getTen() {
            return ten;
        }
        public void setTen(String ten) {
            this.ten = ten;
        }
        @Override
        public String toString() {
            // TODO Auto-generated method stub
            return this.ma+" - "+this.ten;
        }
}
