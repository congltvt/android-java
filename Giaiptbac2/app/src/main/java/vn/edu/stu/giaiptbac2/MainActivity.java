package vn.edu.stu.giaiptbac2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText etA, etB, etC;
    TextView txtKetqua;
    Button btnTimnghiem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addControls();
        addEvents();
    }

    private void addControls() {
        etA = findViewById(R.id.etA);
        etB = findViewById(R.id.etB);
        etC = findViewById(R.id.etC);
        txtKetqua = findViewById(R.id.txtKetqua);
        btnTimnghiem = findViewById(R.id.btnTimnghiem);
    }

    private void addEvents() {
        btnTimnghiem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double soA = Double.parseDouble(etA.getText().toString());
                double soB = Double.parseDouble(etB.getText().toString());
                double soC = Double.parseDouble(etC.getText().toString());
                double denta = ((soB * soB)- 4*soA*soC);

                if(denta < 0){
                    txtKetqua.setText("Phương trình vô nghiệm");
                } else if(denta == 0){
                    txtKetqua.setText("Phương trình có nghiệm kép x1, x2= " + (-soB/(2*soA)));
                }else {
                    txtKetqua.setText("Phương trình có hai nghiệm phân biệt: x1= " + ((-soB+Math.sqrt(denta))/2*soA) + ", x2= "
                            + ((-soB-Math.sqrt(denta))/2*soA));
                }
            }
        });
    }
}