package vn.edu.stu.thuchanhbuoi5bt1;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import vn.edu.stu.thuchanhbuoi5bt1.model.Congviec;

public class MainActivity extends AppCompatActivity {
    FloatingActionButton fabThem;
    ArrayAdapter<Congviec> adapter;
    ListView lvCongviec;
    Congviec chon;
    int requestCode = 113, resultCode = 115;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addControls();
        addEvents();
    }

    private void addControls() {
        fabThem = findViewById(R.id.fabThem);
        adapter = new ArrayAdapter<>(
                MainActivity.this,
                android.R.layout.simple_list_item_1
        );
        lvCongviec = findViewById(R.id.lvCongviec);
        lvCongviec.setAdapter(adapter);
        chon = null;
    }

    private void addEvents() {
        fabThem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(
                        MainActivity.this, activity_them_sua_congviec.class
                );
                startActivityForResult(intent, requestCode);
            }
        });
        lvCongviec.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position , long id) {
                if(position >= 0 && position < adapter.getCount()){
                    Intent intent = new Intent(
                            MainActivity.this,
                            activity_them_sua_congviec.class
                    );
                    chon = adapter.getItem(position);
                    intent.putExtra("CHON", chon);
                    startActivityForResult(intent, requestCode);
                }
            }
        });
        lvCongviec.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long id) {
             return true;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
      if(requestCode == this.requestCode){
          if(resultCode == this.requestCode){
              if(data.hasExtra("TRA")){
                  Congviec tra = (Congviec) data.getSerializableExtra("TRA");
                  if(chon == null){
                      adapter.add(tra);
                  }else {
                      chon.setTen(tra.getTen());
                      chon.setHan(tra.getHan());
                  }
                  adapter.notifyDataSetChanged();
              }
          }
      }
      chon = null;
    }
}