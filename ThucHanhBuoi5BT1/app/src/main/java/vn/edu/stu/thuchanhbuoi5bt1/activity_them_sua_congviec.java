package vn.edu.stu.thuchanhbuoi5bt1;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

import util.FormatUtil;
import vn.edu.stu.thuchanhbuoi5bt1.model.Congviec;

public class activity_them_sua_congviec extends AppCompatActivity {
    EditText tvTen;
    TextView tvNgay, tvGio;
    ImageButton btnDatePicker, btnTimePicker;
    Button btnLuu;
    Calendar calendar;
    Congviec chon;

    int result = 115;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_them_sua_congviec);

        addControls();
        addEvents();
        getIntentData();
    }

    private void addControls() {
        tvTen = findViewById(R.id.tvTen);
        tvNgay = findViewById(R.id.tvNgay);
        tvGio = findViewById(R.id.tvGio);
        btnDatePicker = findViewById(R.id.btnDatePicker);
        btnTimePicker = findViewById(R.id.btnTimePicker);
        btnLuu = findViewById(R.id.btnLuu);
        calendar = Calendar.getInstance();
        chon = null;
    }

    private void addEvents() {
        btnDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                xuLyChonNgay();
            }


        });
        btnTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                xuLyChonGio();
            }
        });

        btnLuu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                xuLyLuu();
            }
        });
    }

    private void xuLyLuu() {
        if(chon == null){
            chon = new Congviec();
        }
        chon.setTen(tvTen.getText().toString());
        chon.setHan(calendar.getTime());

        Intent intent = getIntent() ;
        intent.putExtra("TRA", chon);
        setResult(result, intent);
        finish();
    }

    private void xuLyChonGio() {
        TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar.set(Calendar.MINUTE, minute);
                tvNgay.setText(FormatUtil.formatDate(calendar.getTime()));
            }
        };
        TimePickerDialog timePickerDialog = new TimePickerDialog(activity_them_sua_congviec.this,
                listener,
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE), false);
        timePickerDialog.show();
    }

    private void xuLyChonNgay() {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMOnth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DATE, dayOfMOnth);
                tvNgay.setText(FormatUtil.formatDate(calendar.getTime()));
            }
        };
      DatePickerDialog datePickerDialog = new DatePickerDialog(activity_them_sua_congviec.this,
              listener,
              calendar.get(Calendar.YEAR),
              calendar.get(Calendar.MONTH),
              calendar.get(Calendar.DATE)
      );
      datePickerDialog.show();
    }


    private void getIntentData() {
        Intent intent = getIntent();
        if(intent.hasExtra("CHON")){
            chon = (Congviec) intent.getSerializableExtra("CHON");
            if(chon != null){
                tvTen.setText(chon.getTen());
                calendar.setTime(chon.getHan());
                tvNgay.setText(FormatUtil.formatDate(calendar.getTime()));
                tvGio.setText(FormatUtil.formatDate(calendar.getTime()));
            }else {
                resetView();
            }
        }else {
            resetView();
        }
    }

    private void resetView() {
        tvTen.setText("");
        tvNgay.setText("đ/MM/yyyy");
        tvGio.setText("hh:mm aa");
        calendar = Calendar.getInstance();
        chon = null;
    }
}