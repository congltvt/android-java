package com.example.lap_7.;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.qlsach.model.Sach;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    com.example.lap_7.util.AppDatabase db;
    ArrayList<Sach> dsSach;
    ArrayAdapter<Sach> adapter;
    ListView lvSach;
    FloatingActionButton fabThem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        com.example.lap_7.util.DBConfigUtil.copyDatabaseFromAssets(MainActivity.this);


    }
    private void addControls(){
        db= com.example.lap_7.util.AppDatabase.getAppDatabase(this);
        dsSach=new ArrayList<>();
        adapter = new ArrayAdapter<>(
                MainActivity.this,
                android.R.layout.simple_list_item_1,dsSach
        );
        lvSach=findViewById(R.id.lvSach);
        lvSach.setAdapter(adapter);
        fabThem=findViewById(R.id.fabThem);
    }private void hienthiDanhSach(){
        dsSach.clear();
        dsSach.addAll(db.sachDao().getAll());
        adapter.notifyDataSetChanged();
    }
    private void addEvents(){
        lvSach.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                        if(position>=0 && position <dsSach.size()){
                            Sach sach=dsSach.get(position);
                            int deleteRowCount=db.sachDao().delete(sach);
                            if(deleteRowCount>0){
                                dsSach.remove(position);
                                adapter.notifyDataSetChanged();
                            }
                        }
                        return false;
                    }
                }
        );
    }


}