package com.example.qlsach.util;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.lap_7.dao.SachDao;
import com.example.lap_7.model.Sach;
import com.example.lap_7.util.DBConfigUtil;

@Database(entities = {Sach.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE;

    public abstract SachDao sachDao();

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, DBConfigUtil.DATABASE_NAME)
                            // Dòng allowMainThreadQueries() cho phép truy vấn
                            // trên MainThread. Nếu dữ liệu nhỏ thì được, nhưng
                            // nếu dữ liệu lớn có thể làm cho ứng dụng bị treo
                            // lúc truy vấn. Vì vậy chỉ nên allow trong các
                            // ứng dụng demo, còn ứng dụng thực tế với
                            // dữ liệu lớn thì không nên allow, lúc này các
                            // truy vấn nên thực hiện bằng AsyncTask
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
