package com.example.lap_7.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
@Entity(tableName = "Sach_TBL")
public class Sach {
    @PrimaryKey(autoGenerate = true)
    private int ma;

    private String ten;

    private int tacgia;
    @ColumnInfo(name = "namxuatban")
    private int namXuatBan;

    public Sach(){
    }

    public int getMa() {
        return ma;
    }

    public void setMa(int ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public int getTacgia() {
        return tacgia;
    }

    public void setTacgia(int tacgia) {
        this.tacgia = tacgia;
    }

    public int getNamXuatBan() {
        return namXuatBan;
    }

    public void setNamXuatBan(int namXuatBan) {
        this.namXuatBan = namXuatBan;
    }

    @Override
    public String toString() {
        return "Sach{" +
                "Ma=" + ma +
                "Ten='" + ten +
                "Tacgia=" + tacgia +
                "NamXuatBan=" + namXuatBan;
    }
}
