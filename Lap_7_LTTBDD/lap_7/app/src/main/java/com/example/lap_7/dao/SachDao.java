package com.example.lap_7.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Query;

import com.example.lap_7.model.Sach;

import java.util.List;
@Dao
public interface SachDao {
    @Query("SELECT*FROM Sach_TBL")
    List<Sach> getAll();
    @Delete
    int delete(Sach sach);
}
