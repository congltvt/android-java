package vn.edu.stu.luongthanhcong_dh51905352.model;

public class CurrencyRate {
    private String code;
    private double rate;

    public CurrencyRate(String code, double rate) {
        this.code = code;
        this.rate = rate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "Code: " + code + '\n' +
                "Rate: " + rate;
    }
}
