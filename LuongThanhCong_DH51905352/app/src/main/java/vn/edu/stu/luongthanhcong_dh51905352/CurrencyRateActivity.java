package vn.edu.stu.luongthanhcong_dh51905352;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import vn.edu.stu.luongthanhcong_dh51905352.model.CurrencyRate;

public class CurrencyRateActivity extends AppCompatActivity {
    final String SERVER = "http://document.fitstu.net/2022/ws1.php";
    EditText etType;
    Button btnGetRate;
    ArrayList<CurrencyRate> dsTiente;
    ArrayAdapter<CurrencyRate> adapterTT;
    ListView lvTiente;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency_rate);

        addControls();
        addEvents();
    }

    private void addControls() {
        etType = findViewById(R.id.etType);
        btnGetRate = findViewById(R.id.btnGetRate);
        lvTiente = findViewById(R.id.lvTiente);
        dsTiente = new ArrayList<>();
        adapterTT = new ArrayAdapter<>(CurrencyRateActivity.this, android.R.layout.simple_list_item_1, dsTiente);
        lvTiente.setAdapter(adapterTT);
    }

    private void addEvents() {
        btnGetRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                xuLyGetData();
            }
        });
    }

    private void xuLyGetData() {
        RequestQueue requestQueue = Volley.newRequestQueue(CurrencyRateActivity.this);
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(
                        CurrencyRateActivity.this,
                        response,
                        Toast.LENGTH_LONG
                ).show();
                try{
                    dsTiente.clear();
                    JSONObject data = new JSONObject(response);
                    JSONArray jsonArray = data.getJSONArray("RETURN");
                    int len= jsonArray.length();
                    for(int i=0; i<len; i++){
                        JSONObject jsonObject= jsonArray.getJSONObject(i);
                        String code= jsonObject.getString("code");
                        double rate = Double.parseDouble(jsonObject.getString("rate"));
                        dsTiente.add(new CurrencyRate(code, rate));
                    }
                    adapterTT.notifyDataSetChanged();
                    Toast.makeText(
                            CurrencyRateActivity.this,
                            "Lấy dữ liệu thành công",
                            Toast.LENGTH_SHORT
                    ).show();

                } catch (Exception ex) {
                }
            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(CurrencyRateActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        };
        Uri.Builder builder = Uri.parse(SERVER).buildUpon();
        builder.appendQueryParameter("action","getrate");
        builder.appendQueryParameter("type", etType.getText().toString());
        String url = builder.build().toString();
        StringRequest request = new StringRequest(
                Request.Method.GET,
                url,
                responseListener,
                errorListener
        );
        request.setRetryPolicy(
                new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                )
        );
        requestQueue.add(request);
    }
}