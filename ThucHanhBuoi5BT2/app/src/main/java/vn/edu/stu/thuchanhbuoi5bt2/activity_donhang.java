package vn.edu.stu.thuchanhbuoi5bt2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import vn.edu.stu.thuchanhbuoi5bt2.model.MaHang;
public class activity_donhang extends AppCompatActivity {
    ArrayAdapter<MaHang> adapter;
    ListView lvDonhang;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donhang);
        addControls();
        getIntentData();
    }

    private void addControls() {
        adapter = new ArrayAdapter<>(
                activity_donhang.this,
                android.R.layout.simple_list_item_1
        );
        lvDonhang = findViewById(R.id.lvDonhang);
        lvDonhang.setAdapter(adapter);
    }

    private void getIntentData() {
        Intent intent = getIntent();

        if(intent.hasExtra("DONHANG")){
            ArrayList<MaHang> donhang = (ArrayList<MaHang>) intent.getSerializableExtra("DONHANG");
            if(donhang != null){
                adapter.addAll(donhang);
            }
        }
    }
}