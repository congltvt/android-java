package vn.edu.stu.thuchanhbuoi5bt2;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Random;

import vn.edu.stu.thuchanhbuoi5bt2.Adapter.MahangAdapter;
import vn.edu.stu.thuchanhbuoi5bt2.model.MaHang;

public class    MainActivity extends AppCompatActivity {
    FloatingActionButton fabDathang;
    ArrayList<MaHang> dsMaHang;
    MahangAdapter adapter;
    ListView lvMaHang;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addControls();
        addEvents();
        fakeData();
    }

    private void addControls() {
        fabDathang = findViewById(R.id.fabDathang);
        dsMaHang = new ArrayList<>();
        adapter = new MahangAdapter(
                MainActivity.this,
                R.layout.item_mathang,
                dsMaHang
        );
        lvMaHang = findViewById(R.id.lvMaHang);
        lvMaHang.setAdapter(adapter);
    }

    private void addEvents() {
        fabDathang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<MaHang> donhang = new ArrayList<>();
                for(MaHang mh : dsMaHang){
                    if(mh.getSoluong() > 0) {
                        donhang.add(mh);
                    }
                }
                Intent intent = new Intent(
                        MainActivity.this,
                        activity_donhang.class
                );
                intent.putExtra("DONHANG", donhang);
                startActivity(intent);
            }
        });
    }

    private void fakeData() {
        Random rd = new Random();
        for(int i = 0; i < 20; i++){
            dsMaHang.add(new MaHang("Món hàng" + i, rd.nextInt(10000), 0));
        }
        adapter.notifyDataSetChanged();
    }
}