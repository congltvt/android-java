package vn.edu.stu.thuchanhbuoi5bt2.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import vn.edu.stu.thuchanhbuoi5bt2.R;
import vn.edu.stu.thuchanhbuoi5bt2.model.MaHang;
import vn.edu.stu.thuchanhbuoi5bt2.util.FormatUtil;

import java.util.List;

public class MahangAdapter extends ArrayAdapter<MaHang>{
    Activity context;
    int resource;
    List<MaHang> objects;

     public MahangAdapter(Activity context, int resource, List<MaHang> objects){
         super(context,resource,objects);
         this.context = context;
         this.resource = resource;
         this.objects = objects;
     }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = this.context.getLayoutInflater();
        View item = inflater.inflate(this.resource, null);
        TextView tvTen = item.findViewById(R.id.tvTen);
        TextView tvDongia = item.findViewById(R.id.tvDongia);
        EditText etSoluong = item.findViewById(R.id.etSoluong);

        final MaHang mh = this.objects.get(position);
        tvTen.setText(mh.getTen());
        tvDongia.setText(FormatUtil.formatNumber(mh.getDongia()));
        etSoluong.setText(mh.getSoluong() + "");
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                try {
                    mh.setSoluong(Integer.parseInt(charSequence.toString()));
                }catch (Exception ex){
                    mh.setSoluong(0);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
        etSoluong.addTextChangedListener(textWatcher);
        return item;
    }
}
