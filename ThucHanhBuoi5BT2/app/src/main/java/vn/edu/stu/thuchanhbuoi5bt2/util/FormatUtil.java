package vn.edu.stu.thuchanhbuoi5bt2.util;

import java.text.DecimalFormat;

public class FormatUtil {
    static DecimalFormat decimalFormat = new DecimalFormat("#,##0");

    public static String formatNumber(int num){
        return decimalFormat.format(num);
    }
}
