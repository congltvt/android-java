package vn.edu.stu.thuchanhbuoi5bt2.model;

import java.io.Serializable;

import vn.edu.stu.thuchanhbuoi5bt2.util.FormatUtil;

public class MaHang implements Serializable {
    private String ten;
    private int dongia;
    private int soluong;

    public MaHang() {
    }

    public MaHang(String ten, int dongia, int soluong) {
        this.ten = ten;
        this.dongia = dongia;
        this.soluong = soluong;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public int getDongia() {
        return dongia;
    }

    public void setDongia(int dongia) {
        this.dongia = dongia;
    }

    public int getSoluong() {
        return soluong;
    }

    public void setSoluong(int soluong) {
        this.soluong = soluong;
    }

    @Override
    public String toString() {
        return "Tên Hàng: '" + ten + '\n'
               + "Đơn giá: " + FormatUtil.formatNumber(dongia) + "\n"
                +"Số lượng: " + FormatUtil.formatNumber(soluong) + "\n"
                + "Thành Tiền: " + FormatUtil.formatNumber((soluong * dongia));
    }
}
