package com.android.qldc.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.qldc.R;
import com.android.qldc.model.Toy;
import com.android.qldc.model.ToyType;

import java.util.ArrayList;
import java.util.List;

public class ToyTypeAdapter extends BaseAdapter {
    private List<ToyType> typeList = new ArrayList<>();

    public void setTypeList(List<ToyType> typeList){
        this.typeList = typeList;
        notifyDataSetChanged();
    }

    public void deleteType(int position){
        typeList.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return typeList.size();
    }

    @Override
    public ToyType getItem(int i) {
        return typeList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = View.inflate(viewGroup.getContext(), R.layout.item_toy_type, null);
        ToyType toyType = typeList.get(i);
        ((TextView) view.findViewById(R.id.txt_name)).setText(toyType.getName());
        ((TextView) view.findViewById(R.id.txt_describe)).setText(toyType.getDescribe());
        return view;
    }
}
