package com.android.qldc.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import com.android.qldc.DatabaseHelper;
import com.android.qldc.R;
import com.android.qldc.adapter.ToyAdapter;

public class ToyFragment extends Fragment {
    private DatabaseHelper database;
    private ListView listView;
    private Button button;
    private ToyAdapter toyAdapter = new ToyAdapter();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment, container, false);
        listView = v.findViewById(R.id.list);
        button = v.findViewById(R.id.button_add);
        listView.setAdapter(toyAdapter);
        addListener();
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        database = new DatabaseHelper(context);
    }

    @Override
    public void onStart() {
        super.onStart();
        toyAdapter.setToyList(database.getAllToy(), database.getAllToyType());
    }

    private void addListener(){
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DetailToyActivity.openDetailToyActivity(getActivity(), i);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                database.deleteToy(toyAdapter.getItem(i));
                toyAdapter.deleteToy(i);
                return false;
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DetailToyActivity.openDetailToyActivity(getActivity(), DetailToyActivity.ADD_POSITION);
            }
        });
    }

}

