package com.android.qldc.model;

import android.net.Uri;

import com.android.qldc.R;

public class Toy {
    private int id;
    private String name;
    private int idType;
    private int ages;
    private String image;
    private int price;

    public Toy(int id, String name, int idType, int ages, String image, int price) {
        this.id = id;
        this.name = name;
        this.idType = idType;
        this.ages = ages;
        this.image = image;
        this.price = price;
    }

    public Toy(String name, int idType, int ages, String image, int price) {
        this.id = id;
        this.name = name;
        this.idType = idType;
        this.ages = ages;
        this.image = image;
        this.price = price;
    }

    public Toy(String name, int idType, int ages, int image, int price) {
        this.id = id;
        this.name = name;
        this.idType = idType;
        this.ages = ages;
        this.image = getURLForResource(image);
        this.price = price;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIdType() {
        return idType;
    }

    public void setIdType(int idType) {
        this.idType = idType;
    }

    public int getAges() {
        return ages;
    }

    public void setAges(int ages) {
        this.ages = ages;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Uri getImageUri() {
        return Uri.parse(image);
    }

    public String getURLForResource (int resourceId) {
        return Uri.parse("android.resource://" + R.class.getPackage().getName() + "/" + resourceId).toString();
    }
}
