package com.android.qldc;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.android.qldc.model.Toy;
import com.android.qldc.model.ToyType;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Database Name
    public static final String DATABASE_NAME = "qldc";
    private static final int DATABASE_VERSION = 1;

    // Table Names
    private static final String TABLE_TOY = "toy";
    private static final String TABLE_TOY_TYPE = "type";

    // Common column names
    private static final String KEY_ID_TOY = "id";

    // Table toy
    private static final String KEY_NAME_TOY = "nametoy";
    private static final String KEY_TYPE_ID = "idType";
    private static final String KEY_AGES = "ages";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_PRICE = "price";

    //Table type
    private static final String KEY_ID_TYPE = "id";
    private static final String KEY_NAME_TYPE = "nametype";
    private static final String KEY_DES = "describe";

    private static final String SQL_CREATE_TABLE_TOY = "CREATE TABLE "
            + TABLE_TOY + "(" + KEY_ID_TOY + " INTEGER PRIMARY KEY,"
            + KEY_NAME_TOY + " TEXT,"
            + KEY_TYPE_ID + " INTEGER,"
            + KEY_AGES + " INTEGER,"
            + KEY_IMAGE + " TEXT,"
            + KEY_PRICE + " INTEGER" + ")";
    private static final String SQL_CREATE_TABLE_TOY_TYPE = "CREATE TABLE "
            + TABLE_TOY_TYPE + "(" + KEY_ID_TYPE + " INTEGER PRIMARY KEY,"
            + KEY_NAME_TYPE + " TEXT,"
            + KEY_DES + " TEXT" + ")";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_TOY);
        db.execSQL(SQL_CREATE_TABLE_TOY_TYPE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TOY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TOY_TYPE);

        // create new tables
        onCreate(db);
    }

    public void addToyType(ToyType toyType) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME_TYPE, toyType.getName());
        values.put(KEY_DES, toyType.getDescribe());
        db.insert(TABLE_TOY_TYPE, null, values);
    }

    public void deleteToyType(ToyType toyType) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_TOY_TYPE, KEY_ID_TYPE + " = ?", new String[]{String.valueOf(toyType.getId())});
    }

    public void updateToyType(ToyType toyType) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME_TYPE, toyType.getName());
        values.put(KEY_DES, toyType.getDescribe());
        db.update(TABLE_TOY_TYPE, values, KEY_ID_TYPE + " = ?", new String[]{String.valueOf(toyType.getId())});
    }

    public ArrayList<ToyType> getAllToyType() {
        ArrayList<ToyType> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLE_TOY_TYPE,
                null, null, null, null, null, null);
        if (cursor != null) cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            int id = cursor.getInt(0);
            String name = cursor.getString(1);
            String describe = cursor.getString(2);
            list.add(new ToyType(id, name, describe));
            cursor.moveToNext();
        }
        return list;
    }

    public void addToy(Toy toy) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME_TOY, toy.getName());
        values.put(KEY_TYPE_ID, toy.getIdType());
        values.put(KEY_AGES, toy.getAges());
        values.put(KEY_IMAGE, toy.getImage());
        values.put(KEY_PRICE, toy.getPrice());
        db.insert(TABLE_TOY, null, values);
    }

    public void deleteToy(Toy toy) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_TOY, KEY_ID_TOY + " = ?", new String[]{String.valueOf(toy.getId())});
    }

    public void updateToy(Toy toy) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME_TOY, toy.getName());
        values.put(KEY_TYPE_ID, toy.getIdType());
        values.put(KEY_AGES, toy.getAges());
        values.put(KEY_IMAGE, toy.getImage());
        values.put(KEY_PRICE, toy.getPrice());
        db.update(TABLE_TOY, values, KEY_ID_TOY + " = ?", new String[]{String.valueOf(toy.getId())});
    }

    public ArrayList<Toy> getAllToy() {
        ArrayList<Toy> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLE_TOY,
                null, null, null, null, null, null);
        if (cursor != null) cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            int id = cursor.getInt(0);
            String name = cursor.getString(1);
            int idType = cursor.getInt(2);
            int ages = cursor.getInt(3);
            String image = cursor.getString(4);
            int price = cursor.getInt(5);
            list.add(new Toy(id, name, idType, ages, image, price));
            cursor.moveToNext();
        }
        return list;
    }

    public ArrayList<Toy> getToyofType(int id_Type) {
        ArrayList<Toy> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLE_TOY,
                null, KEY_TYPE_ID + "=" + id_Type, null, null, null, null);
        if (cursor != null) cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            int id = cursor.getInt(0);
            String name = cursor.getString(1);
            int idType = cursor.getInt(2);
            int ages = cursor.getInt(3);
            String image = cursor.getString(4);
            int price = cursor.getInt(5);
            list.add(new Toy(id, name, idType, ages, image, price));
            cursor.moveToNext();
        }
        return list;
    }

    public ArrayList<Toy> getListToy() {
        ArrayList<Toy> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLE_TOY, null,
                KEY_AGES + "< 3" + " AND " + KEY_PRICE + "< 100000" + " AND " + KEY_PRICE + "> 50000",
                null, null, null, null);

        if (cursor != null) cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            int id = cursor.getInt(0);
            String name = cursor.getString(1);
            int idType = cursor.getInt(2);
            int ages = cursor.getInt(3);
            String image = cursor.getString(4);
            int price = cursor.getInt(5);
            list.add(new Toy(id, name, idType, ages, image, price));
            cursor.moveToNext();
        }
        return list;
    }

    // closing database
    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

}