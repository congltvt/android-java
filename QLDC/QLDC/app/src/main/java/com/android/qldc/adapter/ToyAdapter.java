package com.android.qldc.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.qldc.R;
import com.android.qldc.model.Toy;
import com.android.qldc.model.ToyType;

import java.util.ArrayList;
import java.util.List;

public class ToyAdapter extends BaseAdapter {
    private List<Toy> toyList = new ArrayList<>();
    private List<ToyType> typeList = new ArrayList<>();

    public void setToyList(List<Toy> toyList, List<ToyType> typeList){
        this.toyList = toyList;
        this.typeList = typeList;
        notifyDataSetChanged();
    }

    public void deleteToy(int position){
        toyList.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return toyList.size();
    }

    @Override
    public Toy getItem(int i) {
        return toyList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = View.inflate(viewGroup.getContext(), R.layout.item_toy, null);
        Toy toy = toyList.get(i);
        String nameType = "";
        for (int j = 0; j < typeList.size(); j++ ){
            if (toy.getIdType() == typeList.get(j).getId()){
                nameType = typeList.get(j).getName();
            }
        }
        ((TextView) view.findViewById(R.id.txt_name)).setText(toy.getName());
        ((TextView) view.findViewById(R.id.txt_ages)).setText(String.valueOf(toy.getAges()));
        ((TextView) view.findViewById(R.id.txt_type)).setText(nameType);
        ((TextView) view.findViewById(R.id.txt_price)).setText(String.valueOf(toy.getPrice()));
        ((ImageView) view.findViewById(R.id.image)).setImageURI(toy.getImageUri());
        return view;
    }
}
