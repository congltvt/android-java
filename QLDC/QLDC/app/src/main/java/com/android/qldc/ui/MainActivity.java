package com.android.qldc.ui;

import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.util.Log;

import com.android.qldc.DatabaseHelper;
import com.android.qldc.adapter.PagerAdapter;
import com.android.qldc.R;
import com.android.qldc.model.Toy;
import com.android.qldc.model.ToyType;
import com.google.android.material.tabs.TabLayout;

import static com.google.android.material.tabs.TabLayout.*;

public class MainActivity extends FragmentActivity {
    TabLayout tabLayout;
    ViewPager pager;
    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new DatabaseHelper(getApplicationContext());
        initToyList();
        initTypeList();
        
        tabLayout = findViewById(R.id.tablayout);
        pager = findViewById(R.id.pager);

        tabLayout.addTab(tabLayout.newTab().setText("Đồ chơi"));
        tabLayout.addTab(tabLayout.newTab().setText("Loại đồ chơi"));
        tabLayout.setTabGravity(GRAVITY_FILL);

        pager.setAdapter(new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount()));
        pager.addOnPageChangeListener(new TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new OnTabSelectedListener() {
            @Override
            public void onTabSelected(Tab tab) {
                pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(Tab tab) {

            }

            @Override
            public void onTabReselected(Tab tab) {

            }
        });

    }

    public void initTypeList() {
        if (db.getAllToyType().size() == 0) {
            db.addToyType(new ToyType("Do nhua", "abc"));
            db.addToyType(new ToyType("Do nhua", "abc"));
        }
    }

    public void initToyList() {
        if (db.getAllToy().size() == 0) {
            db.addToy(new Toy("abc", 001, 3, R.drawable.img_psyduck, 1122));
            db.addToy(new Toy("abc", 001, 3, R.drawable.img_psyduck, 1122));
            db.addToy(new Toy("abc", 001, 3, R.drawable.img_psyduck, 1122));
        }
    }
}