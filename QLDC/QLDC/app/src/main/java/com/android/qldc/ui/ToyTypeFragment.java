package com.android.qldc.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import com.android.qldc.DatabaseHelper;
import com.android.qldc.R;
import com.android.qldc.adapter.ToyTypeAdapter;

public class ToyTypeFragment extends Fragment {
    private DatabaseHelper database;
    private ListView listView;
    private Button button;
    private ToyTypeAdapter typeAdapter = new ToyTypeAdapter();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment, container, false);
        listView = v.findViewById(R.id.list);
        button = v.findViewById(R.id.button_add);
        listView.setAdapter(typeAdapter);
        addListener();
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        database = new DatabaseHelper(context);
    }

    @Override
    public void onStart() {
        super.onStart();
        typeAdapter.setTypeList(database.getAllToyType());
    }

    private void addListener(){
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DetailToyTypeActivity.openDetailTypeActivity(getActivity(), i);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                database.deleteToyType(typeAdapter.getItem(i));
                typeAdapter.deleteType(i);
                return false;
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DetailToyTypeActivity.openDetailTypeActivity(getActivity(), DetailToyTypeActivity.ADD_POSITION);
            }
        });
    }
}