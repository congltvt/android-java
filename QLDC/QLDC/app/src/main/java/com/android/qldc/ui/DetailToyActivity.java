package com.android.qldc.ui;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.qldc.DatabaseHelper;
import com.android.qldc.R;
import com.android.qldc.adapter.ToyTypeAdapter;
import com.android.qldc.model.Toy;
import com.android.qldc.model.ToyType;

import java.util.ArrayList;
import java.util.List;

public class DetailToyActivity extends AppCompatActivity {

    public static final String EXTRA_POSITION = "EXTRA_POSITION";
    public static final int ADD_POSITION = -1;
    public static boolean EDIT = false;

    public static void openDetailToyActivity(Activity activity, int position) {
        Intent intent = new Intent(activity, DetailToyActivity.class);
        intent.putExtra(EXTRA_POSITION, position);
        activity.startActivity(intent);
    }

    private DatabaseHelper database;
    private Toy toy;
    private ToyType toyType;
    private String img = "";
    private TextView inputName;
    private TextView inputAges;
    private TextView inputPrice;
    private TextView inputType;
    private ImageView image;
    private Button buttonUpdate;
    private List<ToyType> toyTypes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_toy);
        database = new DatabaseHelper(this);
        toyTypes = new ArrayList<>();
        toyTypes = database.getAllToyType();

        int position = getIntent().getIntExtra(EXTRA_POSITION, ADD_POSITION);
        if (position != ADD_POSITION) {
            toy = database.getAllToy().get(position);
            EDIT = true;
        } else EDIT = false;
        addViews();
        addListeners();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            getContentResolver().takePersistableUriPermission(uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
            img = uri.toString();
            image.setImageURI(uri);
        }
    }

    private void addViews() {
        buttonUpdate = findViewById(R.id.button_update);
        inputName = findViewById(R.id.input_name);
        inputAges = findViewById(R.id.input_ages);
        inputPrice = findViewById(R.id.input_price);
        inputType = findViewById(R.id.tvChooseType);
        image = findViewById(R.id.image);
        if (EDIT) {
            String type = "";
            for (int i = 0; i < toyTypes.size(); i++){
                if (toy.getIdType() == toyTypes.get(i).getId()){
                    type = toyTypes.get(i).getName();
                }
            }
            inputName.setText(toy.getName());
            inputAges.setText(String.valueOf(toy.getAges()));
            inputPrice.setText(String.valueOf(toy.getPrice()));
            inputType.setText(type);
            buttonUpdate.setText("Update");
            image.setImageURI(toy.getImageUri());
        } else {
            buttonUpdate.setText("Add");
        }
    }

    private void addListeners() {
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), 2);
            }
        });
        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (EDIT) editToy();
                else addToy();
            }
        });

        inputType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(DetailToyActivity.this);
                LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
                View content = inflater.inflate(R.layout.custom_dialog_type, null);
                dialog.setContentView(content);
                ListView listView = content.findViewById(R.id.lv_type);

                ToyTypeAdapter typeAdapter = new ToyTypeAdapter();
                typeAdapter.setTypeList(toyTypes);
                listView.setAdapter(typeAdapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        toyType = toyTypes.get(i);
                        inputType.setText(toyType.getName());
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }
    private void addToy() {
        Toy s = new Toy(inputName.getText().toString(),
                Integer.parseInt(inputAges.getText().toString()),
                toyType.getId(),
                img,
                Integer.parseInt(inputPrice.getText().toString()));
        database.addToy(s);
        onBackPressed();
        database.closeDB();
    }
    private void editToy() {
        toy.setName(inputName.getText().toString());
        toy.setAges(Integer.parseInt(inputAges.getText().toString()));
        toy.setPrice(Integer.parseInt(inputPrice.getText().toString()));
        toy.setImage(img);
        database.updateToy(toy);
        onBackPressed();
        database.closeDB();
    }
}
