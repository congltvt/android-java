package com.android.qldc.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.android.qldc.ui.ToyFragment;
import com.android.qldc.ui.ToyTypeFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {

    int countTab;

    public PagerAdapter(FragmentManager fm, int countTab) {
        super(fm);
        this.countTab = countTab;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                ToyFragment toyFragment = new ToyFragment();
                return toyFragment;
            case 1:
                ToyTypeFragment toyTypeFragment = new ToyTypeFragment();
                return toyTypeFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return countTab;
    }
}
