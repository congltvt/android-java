package vn.edu.stu.giaiphuongtrinh;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText txtA, txtB;
    Button btnTimNghiem;
    TextView txtKetQua;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addControls();
        addEvents();
    }

    private void addControls() {
        txtA = findViewById(R.id.txtA);
        txtB = findViewById(R.id.txtB);
        btnTimNghiem = findViewById(R.id.btnTimNghiem);
        txtKetQua = findViewById(R.id.txtKetQua);
    }

    private void addEvents() {
        btnTimNghiem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double soA = Double.parseDouble(txtA.getText().toString());
                double soB = Double.parseDouble(txtB.getText().toString());
                if(soA == 0){
                    if(soB != 0) txtKetQua.setText("Phương trình vô nghiệm");
                    else txtKetQua.setText("Phương trình có vô số nghiệm");
                }else
                    txtKetQua.setText("Phương trình có nghiệm x= " + (-soB/soA));
            }
        });
    }
}