package adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.List;

import model.Nhanvien;
import vn.edu.stu.thuchanhbuoi4b3.R;

public class NhanvienAdapter extends ArrayAdapter<Nhanvien> {
    Activity context;
    int resource;
    List<Nhanvien> objects;

    public NhanvienAdapter(
            Activity context,
            int resource,
            List<Nhanvien> objects){
        super(context,resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = this.context.getLayoutInflater();

        View item = inflater.inflate(this.resource, null);

        TextView txtMa = item.findViewById(R.id.txtMa);
        TextView txtTen = item.findViewById(R.id.txtTen);
        TextView txtSdt = item.findViewById(R.id.txtSDT);

        Nhanvien nv = this.objects.get(position);
        txtMa.setText(nv.getMa());
        txtTen.setText(nv.getTen());
        txtSdt.setText(nv.getSdt());
        return item;
    }
}
