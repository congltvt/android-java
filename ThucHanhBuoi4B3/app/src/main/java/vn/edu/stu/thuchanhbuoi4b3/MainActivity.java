package vn.edu.stu.thuchanhbuoi4b3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

import adapter.NhanvienAdapter;
import model.Nhanvien;

public class MainActivity extends AppCompatActivity {

    EditText txtMa, txtTen, txtSdt;
    Button btnLuu;
    ArrayList<Nhanvien> dsNhanvien;
    NhanvienAdapter adapter;
    ListView lvDSNhanvien;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addControls();
        addEvents();
    }

    private void addControls() {
        txtMa = findViewById(R.id.txtMa);
        txtTen = findViewById(R.id.txtTen);
        txtSdt = findViewById(R.id.txtSDT);
        btnLuu = findViewById(R.id.btnLuu);
        dsNhanvien = new ArrayList<>();
        adapter = new NhanvienAdapter(
                MainActivity.this,
                R.layout.item_nhanvien,
                dsNhanvien
        );
        lvDSNhanvien = findViewById(R.id.lvDSNhanVien);
        lvDSNhanvien.setAdapter(adapter);
    }

    private void addEvents() {
        btnLuu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ma = txtMa.getText().toString();
                String ten = txtTen.getText().toString();
                String sdt = txtSdt.getText().toString();
                Nhanvien nv = new Nhanvien(ma, ten, sdt);
                dsNhanvien.add(nv);

                adapter.notifyDataSetChanged();
                txtMa.setText("");
                txtTen.setText("");
                txtSdt.setText("");
                txtMa.requestFocus();
            }
        });

        lvDSNhanvien.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position , long id) {
                if(position >= 0 && position < dsNhanvien.size()){
                    dsNhanvien.remove(position);
                    adapter.notifyDataSetChanged();
                }
                return true;
            }
        });
    }
}