package vn.edu.stu.model;

public class SinhVien {
    private int masv;
    private String tensv;

    public SinhVien() {
    }

    public SinhVien(int masv, String tensv) {
        this.masv = masv;
        this.tensv = tensv;
    }

    public int getMasv() {
        return masv;
    }

    public void setMasv(int masv) {
        this.masv = masv;
    }

    public String getTensv() {
        return tensv;
    }

    public void setTensv(String tensv) {
        this.tensv = tensv;
    }

    @Override
    public String toString() {
        return "Mã=" + masv + "\n Tên='" + tensv;
    }
}
